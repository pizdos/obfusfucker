﻿using System;
using dnlib.DotNet;
using Microsoft.Win32;

namespace ConsoleApp2
{ //Types, Methods, Parameters, Properties, Fields
    internal static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Select File");

            var dialog = new OpenFileDialog()
            {
                Multiselect = false,
                Filter = "Executable File|*.exe;",
                //InitialDirectory = "\\",
            };
            if (dialog.ShowDialog() != true) 
                return;
            var module = ModuleDefMD.Load(dialog.FileName);
            Execute(module);
            module.Write(Environment.CurrentDirectory + @"\result.exe");
            Console.WriteLine("Complete");
        }

        private static void Execute(ModuleDefMD module)
        {
            Renamer.Renamer.Execute(module: module);
            JunkMethods.Execute(module: module);
        }
    }
}