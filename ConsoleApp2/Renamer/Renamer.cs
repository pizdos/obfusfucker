﻿using System.Diagnostics.CodeAnalysis;
using ConsoleApp2.Renamer.Analizer;
using dnlib.DotNet;

namespace ConsoleApp2.Renamer
{
    public class Renamer
    {
        public static void Execute(ModuleDefMD module)
        {
            foreach (var type in module.Types)
            {
                if (CanRename(type))
                    type.Name = RandomHelpers.RandomString();

                foreach (var m in type.Methods)
                {
                    if (CanRename(m))
                        m.Name = RandomHelpers.RandomString();
                    foreach (var para in m.Parameters)
                        para.Name = RandomHelpers.RandomString();
                }
                foreach (var p in type.Properties)
                {
                    if (CanRename(p))
                        p.Name = RandomHelpers.RandomString();
                }
                foreach (var field in type.Fields)
                {
                    if (CanRename(field))
                        field.Name = RandomHelpers.RandomString();
                }
            }
        }

        public static bool CanRename([NotNull]object obj)
        {
            IAnalizer? analyze = obj switch
            {
                TypeDef => new TypeDefAnalyzer(),
                MethodDef => new MethodDefAnalyzer(),
                EventDef => new EventDefAnalyzer(),
                FieldDef => new FieldDefAnalyzer(),
                _ => null
            };
            return analyze != null && analyze.Execute(obj);
        }
    }
}