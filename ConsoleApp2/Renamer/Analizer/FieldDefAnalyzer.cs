﻿namespace ConsoleApp2.Renamer.Analizer
{
    public class FieldDefAnalyzer : IAnalizer
    {
        public bool Execute(object ctx)
        {
            var field = (dnlib.DotNet.FieldDef)ctx;
            if (field.IsRuntimeSpecialName)
                return false;
            return !field.IsLiteral || !field.DeclaringType.IsEnum;
        }
    }
}