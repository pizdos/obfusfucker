﻿namespace ConsoleApp2.Renamer.Analizer
{
    public interface IAnalizer
    {
        public bool Execute(object ctx);
    }
}