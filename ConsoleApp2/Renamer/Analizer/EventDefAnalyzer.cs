﻿namespace ConsoleApp2.Renamer.Analizer
{
    public class EventDefAnalyzer : IAnalizer
    {
        public bool Execute(object ctx)
        {
            var ev = (dnlib.DotNet.EventDef)ctx;
            return !ev.IsRuntimeSpecialName;
        }
    }
}