﻿namespace ConsoleApp2.Renamer.Analizer
{
    public class MethodDefAnalyzer : IAnalizer
    {
        public bool Execute(object ctx)
        {
            var method = (dnlib.DotNet.MethodDef)ctx;
            if (method.IsRuntimeSpecialName)
                return false;
            return !method.DeclaringType.IsForwarder;
        }
    }
}