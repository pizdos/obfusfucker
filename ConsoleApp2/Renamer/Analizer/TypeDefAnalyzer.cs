﻿namespace ConsoleApp2.Renamer.Analizer
{
    public class TypeDefAnalyzer : IAnalizer
    {
        public bool Execute(object ctx)
        {
            var type = (dnlib.DotNet.TypeDef) ctx;
            if (type.IsRuntimeSpecialName)
                return false;
            return !type.IsGlobalModuleType;
        }
    }
}