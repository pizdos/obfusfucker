﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using dnlib.DotNet;
using dnlib.DotNet.Emit;

namespace ConsoleApp2
{
    public class JunkMethods
    {
        public static void Execute(ModuleDef module)
        {
            foreach (var type in module.Types)
            {
                foreach (var method in type.Methods.ToArray())
                {
                    var strings = CreateReturnMethodDef(RandomHelpers.RandomString(), method);
                    var ints = CreateReturnMethodDef(RandomHelpers.RandomInt(), method);
                    type.Methods.Add(strings);
                    type.Methods.Add(ints);
                }
            }
        }

        private static MethodDef CreateReturnMethodDef([NotNull] object value, IOwnerModule sourceMethod)
        {
            var coreLib = value switch
            {
                int => sourceMethod.Module.CorLibTypes.Int32,
                string => sourceMethod.Module.CorLibTypes.String,
                _ => null
            };
            MethodDef newMethod = new MethodDefUser(RandomHelpers.RandomString(),
                MethodSig.CreateStatic(coreLib),
                MethodImplAttributes.IL | MethodImplAttributes.Managed,
                MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.HideBySig)
            {
                Body = new CilBody()
            };
            if (value is int i)
                newMethod.Body.Instructions.Add(Instruction.Create(OpCodes.Ldc_I4, i));
            else if (value is string s)
                newMethod.Body.Instructions.Add(Instruction.Create(OpCodes.Ldstr, s));
            newMethod.Body.Instructions.Add(new Instruction(OpCodes.Ret));
            return newMethod;
        }
    }
}