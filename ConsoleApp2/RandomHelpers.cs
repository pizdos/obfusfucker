﻿using System;
using System.Linq;

namespace ConsoleApp2
{
    public static class RandomHelpers
    {
        public static string RandomString()
        {
            const string chars = "ABCDEF";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, 10)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        
        public static int RandomInt()
        {
            return new Random().Next(0, 99999999);
        }
    }
}