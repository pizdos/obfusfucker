﻿using System;

namespace ConsoleApplication1.Subjects
{
    public class User
    {
        public Guid Id { get; }

        public readonly string Name;

        public readonly int Level;

        public User(Guid id, string name, int level)
        {
            Id = id;
            Name = name;
            Level = level;
        }
    }
}