﻿namespace ConsoleApplication1
{
    public enum Action
    {
        Read,
        Write,
        Change,
        Delete,
        List,
        Execute,
    }
}