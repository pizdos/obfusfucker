﻿using ConsoleApplication1.Objects;
using ConsoleApplication1.Subjects;

namespace ConsoleApplication1
{
    public class Rule
    {
        public readonly User Subject;
        public readonly Action Action;
        public readonly File Object;

        public Rule(User subject, Action action, File o)
        {
            Subject = subject;
            Action = action;
            Object = o;
        }
    }
}