﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication1.Objects;
using ConsoleApplication1.Subjects;

namespace ConsoleApplication1
{
    internal class Program
    {
        private static readonly List<File> Files;

        private static readonly List<User> Users;

        static Program()
        {
            Users = new List<User>
            {
                new User(Guid.Parse("B63DFCAB-906C-44A5-ADFC-CBBC2B4A8CAE"), "administrator", 1000),
                new User(Guid.Parse("B63DFCAB-906C-44A5-ADFC-CBBC2B4A8CAE"), "doctor", 500),
                new User(Guid.Parse("B63DFCAB-906C-44A5-ADFC-CBBC2B4A8CAE"), "registrator", 100),
                new User(Guid.Parse("913B2735-6469-4225-BC26-82AC0B420D30"), "patient", 10),
            };

            Files = new List<File>
            {
                new File("/home/work/patient-card1", 256, 20),
                new File("/home/work/structure", 256, 1),
                new File("/home/work/patient-card2", 256, 20),
                new File("/home/work/hosted-list", 256, 300),
            };
        }

        private static void UsersFiles()
        {
            Console.WriteLine("Users:");
            foreach (var (user, index) in Users.Select((user, index) => (user, index)))
            {
                Console.WriteLine($"{index + 1}. {user.Name} ({user.Level})");
            }

            Console.Write("Select user: ");
            int userIndex;
            while (true)
            {
                var input = Console.ReadLine();
                if (!(
                    !int.TryParse(input, out var userNumber) ||
                    userNumber > Users.Count ||
                    userNumber < 1
                ))
                {
                    userIndex = userNumber - 1;
                    break;
                }

                Console.WriteLine("Incorrect input. Use numbers");
            }

            Console.Write(Environment.NewLine);

            var selectedUser = Users[userIndex];
            Console.WriteLine($"{selectedUser.Name} ({selectedUser.Level}) {selectedUser.Id}");
            Console.WriteLine("Available files");
            foreach (var file in
                Files.Where(file => file.Level <= selectedUser.Level))
            {
                Console.WriteLine($"{file.Path} ({file.SizeBytes}B)");
            }
        }
        
        public static void Main(string[] args)
        {
            for (;;)
            {
                UsersFiles();
                Console.Write(Environment.NewLine);
            }
        }
    }
}